package hospital.view;

import hospital.model.Register;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class HospitalOverviewController {
	@FXML
    private Label lblID;
    @FXML
    private Label lblNumberOfPatients;
    @FXML
    private Label lblMedicaments;
    
  
   
    @FXML
    private void initialize() {
       
        lblID.setText("");
        lblNumberOfPatients.setText("");
        lblMedicaments.setText("");
    }
    
public void showHospitalDetails(Register register) {
    	lblID.setText(String.valueOf(register.getId()));
    	lblNumberOfPatients.setText(String.valueOf(register.getNumberOfPatients()));
    	lblMedicaments.setText(register.getStringMedicaments());
       
    }
}
