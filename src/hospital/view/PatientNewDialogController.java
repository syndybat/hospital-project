package hospital.view;

import hospital.model.Hospital;
import javafx.fxml.FXML;

public class PatientNewDialogController extends PatientEditDialogController {
	private Hospital hospital;
	
	public void setPatient(Hospital hospital) {
        this.hospital = hospital;
        
        tfName.setText("");
        tfSurname.setText("");
        tfMedicaments.setText("");
        tfGroup.setItems(groupValues);
        tfGroup.setValue("A");
    }
    @FXML
    private void handleOk() {
        if (isInputValid()) {
                       	
            	hospital.addPatient(tfName.getText(),tfSurname.getText(), dpBirthday.getValue(),
						tfGroup.getValue(), tfMedicaments.getText());
            	
          
            okClicked = true;
            dialogStage.close();
            
        }
    }

}
