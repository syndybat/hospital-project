package hospital.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import hospital.MainApp;
import hospital.model.Hospital;
import hospital.model.Patient;


public class PatientOverviewController {
	@FXML
    private TextField filterField;
	@FXML
    private TableView<Patient> patientTable;
	@FXML
    private TableColumn<Patient, Integer> idColumn;
    @FXML
    private TableColumn<Patient, String> nameColumn;
    @FXML
    private TableColumn<Patient, String> surnameColumn;

    @FXML
    private Label lblID;
    @FXML
    private Label lblName;
    @FXML
    private Label lblSurname;
    @FXML
    private Label lblGroup;
    @FXML
    private Label lblBirthday;
    @FXML
    private Label lblAmountOfMedicament;
   
    private MainApp mainApp;
    private Hospital hospital;
    private ObservableList<Patient> masterData = FXCollections.observableArrayList();
    FilteredList<Patient> filteredData;
    
    public PatientOverviewController() {
    }

    @FXML
    private void initialize() {
        // Initialize the person table with the two columns.
    	idColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        surnameColumn.setCellValueFactory(cellData -> cellData.getValue().surnameProperty());
        
        
        showPersonDetails(null);
        
        patientTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showPersonDetails(newValue));
        
        
       
    }

   
    public void setMainApp(MainApp mainApp,Hospital hospital) {
        this.mainApp = mainApp;
        this.hospital = hospital;
        
       
        this.masterData=this.hospital.getPatients();
        patientTable.setItems(this.masterData);
        this.filteredData = new FilteredList<>(masterData, p -> true);
        
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(patient -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                try {
                	Integer.parseInt(newValue);
                	if(String.valueOf(patient.getId()).startsWith(newValue)){
                		return true;
                	}
				} catch (Exception e) {
					 String lowerCaseFilter = newValue.toLowerCase();
		                if (patient.getName().toLowerCase().startsWith(lowerCaseFilter)) {
		                    return true; // Filter matches first name.
		                } else if (patient.getSurname().toLowerCase().startsWith(lowerCaseFilter)) {
		                    return true; // Filter matches last name.
		                }
				}
                
               
                return false; // Does not match.
            });
        });
        SortedList<Patient> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(patientTable.comparatorProperty());
        patientTable.setItems(sortedData);
    }
    private void showPersonDetails(Patient patient) {
    	
        if (patient != null) {
          
            lblID.setText(Integer.toString(patient.getId()));
            lblName.setText(patient.getName());
            lblSurname.setText(patient.getSurname());
            lblGroup.setText(patient.getGroup());
            lblBirthday.setText(patient.getBirthday().toString());
            lblAmountOfMedicament.setText(patient.getFormatedMedicament());

           
        } else {
            
        	lblID.setText("");
            lblName.setText("");
            lblSurname.setText("");
            lblGroup.setText("");
            lblBirthday.setText("");
            lblAmountOfMedicament.setText("");
        }
    }
    @FXML
    private void handleDeletePatient() {
        int selectedIndex = patientTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            hospital.deletePatient(selectedIndex);
        } else {
            
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Patient Selected");
            alert.setContentText("Please select a patient in the table.");

            alert.showAndWait();
        }
        
    }
    @FXML
    
    	private void handleNewPerson() {
    	    
    	    mainApp.showPatientNewDialog();
    	    
    }

    @FXML
    private void handleEditPerson() {
        Patient selectedPerson = patientTable.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            boolean okClicked = mainApp.showPatientEditDialog(selectedPerson);
            if (okClicked) {
                showPersonDetails(selectedPerson);
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Person Selected");
            alert.setContentText("Please select a person in the table.");

            alert.showAndWait();
        }
    }
}


