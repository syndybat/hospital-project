package hospital.view;

import java.io.File;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import hospital.MainApp;
import hospital.model.Hospital;

public class RootLayoutController {
	// Reference to the main application
    private Hospital hospital;
    private MainApp mainApp;
    
    public void setMainApp(Hospital hospital,MainApp mainApp) {
        this.hospital = hospital;
        this.mainApp = mainApp;
    }


    
    @FXML
    private void handleSave() {
       hospital.saveData();
    }

   
    @FXML
    private void handleSaveAs() throws IOException {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("document files (*.doc)", "*.doc"));
        
        // Show save file dialog
        File file = fileChooser.showSaveDialog(mainApp.getPrimaryStage());

        if (file != null) {
            hospital.saveData(file.getAbsolutePath());
            }
            
        }
   

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Hospital project");
        alert.setHeaderText("About");
        alert.setContentText("Author: �mejkal Patr\nMade as school project");

        alert.showAndWait();
    }

    
    @FXML
    private void handleExit() {
    	hospital.saveData();
        System.exit(0);
    }
    @FXML
    private void handleHospital() {
    	mainApp.showHospitalOverview();
    }
    @FXML
    private void handlePatients() {
    	mainApp.showPatientOverview();;
    }
}
