package hospital.view;

import hospital.model.Patient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class PatientEditDialogController {
	ObservableList<String> groupValues = FXCollections.observableArrayList("A","B","C");
	@FXML
	protected TextField tfName;
    @FXML
	protected TextField tfSurname;
    @FXML
	protected ComboBox<String> tfGroup;
    @FXML
	protected TextField tfMedicaments;
    @FXML
	protected DatePicker dpBirthday;
    
    protected Stage dialogStage;
    protected Patient patient;
    protected boolean okClicked = false;
    
    
    public boolean isOkClicked() {
        return okClicked;
    }
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    public void setPatient(Patient patient) {
        this.patient = patient;
        
        tfName.setText(patient.getName());
        tfSurname.setText(patient.getSurname());
        tfMedicaments.setText(patient.getMedicament());
        dpBirthday.setValue(patient.getBirthday());
        tfGroup.setItems(groupValues);
        tfGroup.setValue(patient.groupProperty().get());
    }
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            patient.setName(tfName.getText());
            patient.setSurname(tfSurname.getText());
            patient.setGroup(tfGroup.getValue());
            
            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
    
    protected boolean isInputValid() {
    	String message = "";
    	if(tfName.getText()==null)
    		message+="fill name";
    	if(tfSurname.getText()==null)
    		message+=" fill surname";
    	if(tfMedicaments.getText()==null)
    		message+=" fill medicament";
    	if(dpBirthday.getValue()==null)
    		message+=" fill birthday";
    	if(tfGroup.getValue()==null)
    		message+=" fill group";
    	
    	if (message.length() == 0) {
            return true;
        } else {
           
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid input");
            alert.setHeaderText("Please fill the fields");
            alert.setContentText(message);

            alert.showAndWait();

            return false;
    	
    }
    }
    }
