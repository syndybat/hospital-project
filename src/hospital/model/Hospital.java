package hospital.model;



import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;



public class Hospital {
	private LinkedList<Patient> patients = new LinkedList<Patient>();
	private ObservableList<Patient> observablePatients = FXCollections.observableList(patients);
	private Register register = new Register();
	
	public Hospital(){
		loadData();
	}
	
	public void addPatient (String name, String surname, LocalDate birthday,String group,String amoutOfMedicament)
	{	Patient patient;
		
		if(group.contains("C")){
			patient = new CPatient(register.getId(), name, surname, birthday,group,amoutOfMedicament);
			register.calcMedicament(((CPatient)patient).getAmoutOfMedicamentB(), ((CPatient)patient).getAmoutOfMedicamentC());
		}
		else{
			patient = new ABPatient(register.getId(), name, surname, birthday,group,amoutOfMedicament);
			register.calcMedicament(group,((ABPatient)patient).getAmoutOfMedicament());
		}
		
		
		int pos = Collections.binarySearch(patients, patient);
		register.incId();
		register.incPatients();
		if (pos < 0) {
			observablePatients.add(-pos-1,patient);
	     }
		
	}
	
	
	public void deletePatient (int idx)
	{	
		
		if (patients.get(idx).getGroup()=="C")
		{	CPatient patient = (CPatient)patients.get(idx);
			register.calcMedicament(-patient.getAmoutOfMedicamentB(), -patient.getAmoutOfMedicamentC());
		}
		else{
			ABPatient patient = (ABPatient)patients.get(idx);
			register.calcMedicament(patient.getGroup(), -patient.getAmoutOfMedicament());
		}
		observablePatients.remove(idx);
		register.decPatients();
		
	}
	public ObservableList<Patient> getPatients() {
        return observablePatients;
    }
	
	public String toString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (Patient patient : patients) {
			stringBuilder.append(patient.toString()+"\n");
			
		}
		return stringBuilder.toString();
		
	}
	public void saveData ()
	{
		
		FileIO.xmlSaxWriter(patients, register, "source/patients.xml");
		//savePatientsDataToFile(new File("source/patients.xml"));
	}
	public void saveData (String path) throws IOException
	{
		if (path.endsWith(".xml"))
			FileIO.xmlSaxWriter(patients, register, path);
		if(path.endsWith(".txt"))
			FileIO.writeToFile(path, toString());
			
	}
	
	
	public void loadData ()
	{
	
		FileIO.xmlSaxReader("source/patients.xml", this);
	}
	public double[] getMedicaments (){
		return register.getAmountofMedicaments();		
				
	}

	public Register getRegister() {
		return register;
	}
	
}
