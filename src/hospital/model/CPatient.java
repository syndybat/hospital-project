package hospital.model;

import java.time.LocalDate;

import javafx.beans.property.DoubleProperty;
/*
 * C group patient class
 * @param amoutOfMedicamentB
 * @param amoutOfMedicamentC
 */
import javafx.beans.property.SimpleDoubleProperty;

public class CPatient extends Patient {
	
	/**
	 * 
	 */
	
	protected DoubleProperty amoutOfMedicamentB;
	protected DoubleProperty amoutOfMedicamentC;
	
	//Constructor
	public CPatient(int id, String name, String surname, LocalDate birthday,
			String group, String amoutOfMedicament) {
		super(id, name, surname, birthday, group, amoutOfMedicament);
		String[] tmpMedicament = amoutOfMedicament.split("a");
		this.amoutOfMedicamentB= new SimpleDoubleProperty(Double.parseDouble(tmpMedicament[0]));
		this.amoutOfMedicamentC= new SimpleDoubleProperty(Double.parseDouble(tmpMedicament[1]));
	}
	
	//getters setters
	//medicamentB
	public double getAmoutOfMedicamentB() {
		return amoutOfMedicamentB.get();
	}

	public void setAmoutOfMedicamentB(double amoutOfMedicamentB) {
		this.amoutOfMedicamentB.set(amoutOfMedicamentB);
	}
	public DoubleProperty AmoutOfMedicamentPropertyB()
	{
		return amoutOfMedicamentB;
	}
	
	//medicamentC
	public double getAmoutOfMedicamentC() {
		return amoutOfMedicamentC.get();
	}

	public void setAmoutOfMedicamentC(double amoutOfMedicamentC) {
		this.amoutOfMedicamentC.set(amoutOfMedicamentC);
	}
	public DoubleProperty AmoutOfMedicamentPropertyC()
	{
		return amoutOfMedicamentC;
	}
	
	//Methods
	@Override
	public String getMedicament() {
		return String.valueOf(amoutOfMedicamentB.get())+ "a"+String.valueOf(amoutOfMedicamentC.get());
	}

	@Override
	public String getFormatedMedicament() {
		String temp = String.format("%.2f",amoutOfMedicamentB.get());
		temp += "a"+String.format("%.2f",amoutOfMedicamentC.get());
		return temp;
	}

	
}
