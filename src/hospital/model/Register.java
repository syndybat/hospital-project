package hospital.model;

 /*
  * Statistic register of the hospital
  * @param id - id of the last patient
  * @param number - actual number of patient in the hospital
  * @param AmountofMedicaments[] - array of the consumption medicament a day
  */

public class Register   {
	
	private int id;
	private int numberOfPatients;
	private double amountOfMedicaments[]=new double[3];
	
	//Getters setters
	public int getId() {
		return id;
	}
	public void incId() {
		this.id++;
	}
	
	public int getNumberOfPatients() {
		return numberOfPatients;
	}
	public void incPatients() {
		this.numberOfPatients++;
	}
	public void decPatients() {
		this.numberOfPatients--;
	}
	
	public double[] getAmountofMedicaments() {
		return amountOfMedicaments;
	}
	public String getStringMedicaments() {
		String temp = "";
		
			temp=String.format("%.2f;",amountOfMedicaments[0]);
			temp+=String.format("%.2f;",amountOfMedicaments[1]);
			temp+=String.format("%.2f",amountOfMedicaments[2]);
		
		
		return temp;
	}
	
	//Methods
	public void calcMedicament(String group, double medicament){
		if (group.contains("A")) {
			amountOfMedicaments[0]+=medicament;
		}
		else
			amountOfMedicaments[1]+=medicament;	
	}
	public void calcMedicament(double medicament1, double medicament2){
			amountOfMedicaments[1]+=medicament1;
			amountOfMedicaments[2]+=medicament2;
				
	}
	


}
