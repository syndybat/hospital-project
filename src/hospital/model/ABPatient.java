package hospital.model;

import java.time.LocalDate;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class ABPatient extends Patient {

	/**
	 * Patient class group A,B
	 *@param amoutOfMedicament
	 */
	
	protected DoubleProperty amoutOfMedicament;
	
	//Constructor
	public ABPatient(int id, String name, String surname, LocalDate birthday,
			String group,String amoutOfMedicament) {
		super(id, name, surname, birthday, group, amoutOfMedicament);
		this.amoutOfMedicament = new SimpleDoubleProperty(Double.parseDouble(amoutOfMedicament));
	}
	
	//Getters setters
	public double getAmoutOfMedicament() {
		return amoutOfMedicament.get();	
	}
	public DoubleProperty AmoutOfMedicamentProperty()
	{
		return amoutOfMedicament;
	}
	public void setAmoutOfMedicament(double amoutOfMedicament) {
		this.amoutOfMedicament.set(amoutOfMedicament);	
	}
	
	//Methods
	
	@Override
	public String getMedicament() {
		String medicament=String.valueOf(amoutOfMedicament.get());
		return medicament;
	}

	@Override
	public String getFormatedMedicament() {
		String temp = String.format("%.2f",amoutOfMedicament.get());
		return temp;
	}

	
}
