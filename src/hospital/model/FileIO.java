package hospital.model;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.LinkedList;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;



/*
 * File working wrapper
 */


public class FileIO {
	public static LinkedList<String> readFromFile(String path) throws IOException{
		LinkedList<String> list = new LinkedList<String>();
		FileReader in = new FileReader(path);
		BufferedReader reader = new BufferedReader(in);
		String line = reader.readLine();
		while(line != null){
			list.add(line); 
			
			line = reader.readLine();
		}
		reader.close();
		return list;
		
	}

	//write to text file
	public static void writeToFile(String path, String content) throws IOException{
		FileWriter fw = new FileWriter(path);
		BufferedWriter buffW = new BufferedWriter(fw);

		buffW.write(content);
		
		buffW.close();
		fw.close();
	}
	
	// serial write object
	public static void serialWriteToFile(Object obj,String path) throws IOException{
		
		
		FileOutputStream fos = new FileOutputStream(path);
		ObjectOutputStream ous= new ObjectOutputStream(fos);
		ous.writeObject(obj);
		ous.close();
	}
	
	

	//serial read object
	public static Object serialReadFromFile(String path) throws IOException, ClassNotFoundException{
		
		Object obj;
		
		FileInputStream fis = new FileInputStream(path);
		ObjectInputStream ois= new ObjectInputStream(fis);
		
		obj = ois.readObject();
		fis.close();
		
		
		
		return obj;
	}	
		
	
	public static void xmlSaxWriter (LinkedList<Patient> patients,Register register, String path)
	{
		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		XMLStreamWriter xsw = null;
		try
		{
		        xsw =  xof.createXMLStreamWriter(new FileWriter(path));
		        
		        xsw.writeStartDocument();
		        
		        xsw.writeStartElement("Patients");
		        
		        for (Patient patient : patients)
		        {
		                xsw.writeStartElement("Patient");
		                xsw.writeAttribute("id", Integer.toString(patient.getId()));
		                
		                xsw.writeStartElement("Surname");
		                xsw.writeCharacters(patient.getSurname());
		                xsw.writeEndElement();
		                
		                xsw.writeStartElement("Name");
		                xsw.writeCharacters(patient.getName());
		                xsw.writeEndElement();
		                
		                xsw.writeStartElement("Group");
		                xsw.writeCharacters(patient.getGroup());
		                xsw.writeEndElement();
		                
		                xsw.writeStartElement("Birthday");
		                xsw.writeCharacters(patient.getBirthday().toString());
		                xsw.writeEndElement();
		                
		                
		                xsw.writeStartElement("Medicament");
		                xsw.writeCharacters(patient.getMedicament());
		                xsw.writeEndElement();
		                
		                xsw.writeEndElement();
		        }

		        xsw.writeEndElement();
		       
		        xsw.writeEndDocument();
		        xsw.flush();
		        xsw.close();
		}
		catch (Exception e)
		{
		                System.err.println("Error during writing: " + e.getMessage());
		}
		finally
		{
		        try
		        {
		                if (xsw != null)
		                {
		                                xsw.close();
		                }
		        }
		        catch (Exception e)
		        {
		                        System.err.println("Can not close file: " + e.getMessage());
		        }
		}
	}
	
	public static void xmlSaxReader (String path,Hospital hospital)
	{
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader xsr = null;

		try
		{
		        xsr = factory.createXMLStreamReader(new FileReader(path));

		      
		    	String name = "";
		    	String surname = "";
		    	LocalDate birthday = null;
		    	String group = "";
		    	String medicament = "";
		    	String element = "";


		        while(xsr.hasNext())
		        {
		                
		                if (xsr.getEventType() == XMLStreamConstants.START_ELEMENT)
		                {
		                        element = xsr.getName().getLocalPart();
		                        
		                }
		                
		                else if (xsr.getEventType() == XMLStreamConstants.CHARACTERS)
		                {
		                        switch (element)
		                        {
		                                case "Name":
		                                        name= xsr.getText();
		                                        break;
		                                case "Surname":
	                                        surname= xsr.getText();
	                                        break;
		                                case "Birthday":
		                                	birthday= LocalDate.parse(xsr.getText());
	                                        break;
		                                case "Group":
	                                        group= xsr.getText();
	                                        break;
		                                case "Medicament":
	                                        medicament=xsr.getText();
	                                        break;
		                        }
		                        element = "";
		                }
		                
		                else if ((xsr.getEventType() == XMLStreamConstants.END_ELEMENT))
		                {
		                        if ((xsr.getName().getLocalPart().equals("Patient")))
		                        {
		                               
		                        	hospital.addPatient(name, surname, birthday, group, medicament);
		                        	//System.out.println(id+" "+medicament);
		                        }
		                }
		                xsr.next();
		        }

		}
		catch (Exception e)
		{
		        System.err.println("Chyba p�i �ten� souboru: " + e.getMessage());
		        e.printStackTrace();
		}
		finally
		{
		        try
		        {
		                xsr.close();
		        }
		        catch (Exception e)
		        {
		                System.err.println("Chyba p�i uzav�r�n� souboru: " + e.getMessage());
		        }
		}
	}
}
