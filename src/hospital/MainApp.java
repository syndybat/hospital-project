package hospital;


import hospital.model.Hospital;
import hospital.model.Patient;
import hospital.view.HospitalOverviewController;
import hospital.view.PatientEditDialogController;
import hospital.view.PatientNewDialogController;
import hospital.view.PatientOverviewController;
import hospital.view.RootLayoutController;

import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class MainApp extends Application {
	private Stage primaryStage;
    private BorderPane rootLayout;
    private Hospital hospital;
    
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
        this.primaryStage.setTitle("HospitalApp");
        this.hospital = new Hospital();
        this.primaryStage.getIcons().add(new Image("file:source/icon.png"));
        
        initRootLayout();

      showPatientOverview();
	}
	
	public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
           

            RootLayoutController controller = loader.getController();
            controller.setMainApp(hospital,this);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            
        }
    }
	public void showPatientOverview() {
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/PatientOverview.fxml"));
            AnchorPane PatientOverview = (AnchorPane) loader.load();
           
            
            rootLayout.setCenter(PatientOverview);
            
            PatientOverviewController controller = loader.getController();
            controller.setMainApp(this,hospital);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	public void showHospitalOverview() {
        try {
            
        	FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/HospitalOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            
            
            rootLayout.setCenter(page);
            
            /*
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Hospital Statistics");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
*/
            // Set the persons into the controller.
            HospitalOverviewController controller = loader.getController();
            controller.showHospitalDetails(hospital.getRegister());

           // dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }
	public boolean showPatientEditDialog(Patient patient) {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/PatientEditDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        // Set the person into the controller.
	        PatientEditDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);
	        controller.setPatient(patient);

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();

	        return controller.isOkClicked();
	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
	}
	public boolean showPatientNewDialog() {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/PatientNewDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("New Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        // Set the person into the controller.
	        PatientNewDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);
	        controller.setPatient(hospital);

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();

	        return controller.isOkClicked();
	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
	}
	public Stage getPrimaryStage() {
        return primaryStage;
    }
	
	
	public static void main(String[] args) {
		launch(args);
				
	}
	
	
}
